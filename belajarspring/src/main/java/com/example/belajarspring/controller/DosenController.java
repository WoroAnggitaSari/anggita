package com.example.belajarspring.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.example.belajarspring.model.DosenModel;
import com.example.belajarspring.service.DosenService;

@Controller
public class DosenController {
	
	@Autowired // Pengganti Instance = IOC / DI
	private DosenService dosenService;
	
	@RequestMapping(value="tambah_dosen")
	public String menuIsiDosen() {
		String html = "dosen/isi_dosen";
		return html;
	}
	
	@RequestMapping(value="hasil_satu_dosen")
	public String menuSatuDosen(HttpServletRequest request, Model model) {
		String namaDosen = request.getParameter("nama");
		int usiaDosen = Integer.valueOf(request.getParameter("usia"));
		
		DosenModel dosenModel = new DosenModel();
		dosenModel.setNamaDosen(namaDosen);
		dosenModel.setUsiaDosen(usiaDosen);
		
		// transaksi simpan C create
		dosenService.create(dosenModel);  // ketika kodingan ini dijalnkan
		// maka akan menyiman deta ke Object
		
		model.addAttribute("dosenModel", dosenModel);
		
		String html = "dosen/satu_dosen";
		return html;
	}
	
	
	@RequestMapping(value="hasil_banyak_dosen")
	public String menuBanyakDosen(Model model) {
		List<DosenModel> dosenModelList = new ArrayList<DosenModel>();
		dosenModelList = dosenService.read();
		model.addAttribute("dosenModelList", dosenModelList);
		
		String html = "dosen/banyak_dosen";
		return html;
	}
	
	@RequestMapping(value="hasil_banyak_dosen_order_nama")
	public String menuBanyakDosenOrderNama(Model model) {
		List<DosenModel> dosenModelList = new ArrayList<DosenModel>();
		dosenModelList = dosenService.readOrderNama();
		model.addAttribute("dosenModelList", dosenModelList);
		
		String html = "dosen/banyak_dosen_order_nama";
		return html;
	}
	
	@RequestMapping(value="order_dosen")
	public String menuOrderDosen(Model model) {
		List<DosenModel> dosenModelList = new ArrayList<DosenModel>();
		dosenModelList = dosenService.readOrderBy();
		model.addAttribute("dosenModelList", dosenModelList);
		
		String html = "dosen/order_dosen";
		return html;
	}
	
	@RequestMapping(value="cari_nama_dosen")
	public String menuCariNamaDosen(Model model) {
		List<DosenModel> dosenModelList = new ArrayList<DosenModel>();
		dosenModelList = dosenService.readOrderBy();
		model.addAttribute("dosenModelList", dosenModelList);
		String html = "dosen/cari_nama_dosen";
		return html;
	}
	
	@RequestMapping(value="proses_cari_nama")
	public String menuProsesCariNamaDosen(Model model, HttpServletRequest request) {
		String katakunci_nama = request.getParameter("katakunci_nama");
		
		List<DosenModel> dosenModelList = new ArrayList<DosenModel>();
		dosenModelList = dosenService.readWhereNama(katakunci_nama);
		
		model.addAttribute("dosenModelList", dosenModelList);
		String html = "dosen/cari_nama_dosen";
		return html;
	}
	
	
	
	
	
	@RequestMapping("cari_usia")
	public String menuCariUsia() {
	String html = "dosen/Cari_Usia";
		return html;
	}
	
	
	@RequestMapping("proses_cari_usia")
	public String menuProsesCariUsia(HttpServletRequest request, Model model) {
	
		int katakunci = Integer.valueOf(request.getParameter("katakunci_usia"));
		String katakunci_operator = request.getParameter("katakunci_operator");
		
		List<DosenModel> dosenModelList = new ArrayList<DosenModel>();
		dosenModelList = dosenService.CariParameter(katakunci, katakunci_operator);				

		
		
		
		
		model.addAttribute("DosenModelList", dosenModelList);
		
		String html = "dosen/Cari_Usia";
		return html;
	}
	
	
	
	
	//Ubah Dosen
	@RequestMapping("ubah_dosen")
	public String menuUbahdosen() {
	String html = "dosen/ubah";
		return html;
	}
	
	@RequestMapping(value="hasil-ubah-dosen")
	public String menuUpdateDosen(HttpServletRequest request, Model model) {
		String namaDosen = request.getParameter("namaDosen");
		int usiaDosen = Integer.valueOf(request.getParameter("usia"));
		
		DosenModel dosenModel = new DosenModel();
		dosenModel.setNamaDosen(namaDosen);
		dosenModel.setUsiaDosen(usiaDosen);
		
		dosenService.update(dosenModel);
		
		model.addAttribute("dosenModel", dosenModel);
		this.menuBanyakDosen(model);
		
		String html="dosen/banyak_dosen";
		return html;
	}

	
	
	
	
	
	
	
	
	
	//Kodingan Untuk Merubah data
	//Jika melempar data perlu ada Request
	
	@RequestMapping("ubah_dosens")
	public String menuUbahDosensss(HttpServletRequest request, Model model) {
	
		String namaID = request.getParameter("namaID");
		DosenModel dosenModel = new DosenModel();
		
		dosenModel = dosenService.readByString(namaID);
	
		model.addAttribute("dosenModel", dosenModel);
		
		
		
		String html = "dosen/ubah";
		return html;
	}
	
	
	
	// belum
	@RequestMapping("proses_hapus_dosen")
	public String menuhapusDosen(HttpServletRequest request, Model model) {
	
		String namaID = request.getParameter("namaID");
		DosenModel dosenModel = new DosenModel();
		
		dosenModel = dosenService.readByString(namaID);
	
		model.addAttribute("dosenModel", dosenModel);
		
		
		
		String html = "dosen/banyak_dosen";
		return html;
		
	}
	
	
	
}
