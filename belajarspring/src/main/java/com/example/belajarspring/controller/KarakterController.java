package com.example.belajarspring.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.example.belajarspring.model.KarakterModel;

@Controller
public class KarakterController {
	
	@RequestMapping(value="tambah_karakter")
	public String menuIsikarakter() {
		String html = "karakter/isi_karakter";
		return html;
	}
	
	@RequestMapping(value="hasil_satu_karakter")
	public String menuSatukarakter(HttpServletRequest request, Model model) {
		String namaKarakter = request.getParameter("nama");
		int levelKarakter = Integer.valueOf(request.getParameter("level"));
		String statusKarakter = request.getParameter("status");
		
		/*
		 * KarakterModel karakterModel = new KarakterModel();
		 * karakterModel.setNamaKarakter(namaKarakter);
		 * karakterModel.setLevelKarakter(levelKarakter);
		 * karakterModel.setStatusKarakter(statusKarakter);
		 */
		
		model.addAttribute("namaKarakter", namaKarakter);
		model.addAttribute("levelKarakter", levelKarakter);
		model.addAttribute("statusKarakter", statusKarakter);
		
		String html = "karakter/satu_karakter";
		return html;
	}

}
