package com.example.belajarspring.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.example.belajarspring.model.DosenModel;
import com.example.belajarspring.repository.DosenRepository;
import com.example.belajarspring.service.DosenService;

@RestController
@RequestMapping(value="/api/dosen")
public class DosenApi {

	@Autowired
	private DosenService dosenService;
	@Autowired
	private DosenRepository dosenRepository;
	
	@PostMapping("/post")
	@ResponseStatus(code=HttpStatus.CREATED)
	public Map<String, Object> PostApi(@RequestBody DosenModel dosenModel){
		this.dosenService.create(dosenModel);
		Map<String, Object> map= new HashMap<String, Object>();
		map.put("Berhasil", Boolean.TRUE);
		map.put("Notifikasi", "Data Berhasil Disimpan");
		return map;
	}
	
	@GetMapping("/get")
	@ResponseStatus(code=HttpStatus.OK)
	public List<DosenModel> getApi(){
		List<DosenModel> dosenModelList = new ArrayList<DosenModel>();
		dosenModelList = this.dosenService.read();
		return dosenModelList;
	}
	
	@PutMapping("/put")
	@ResponseStatus(code=HttpStatus.OK)
	public Map<String, Object> putApi(@RequestBody DosenModel dosenModel) {
		this.dosenService.update(dosenModel);
		Map<String, Object> map= new HashMap<String, Object>();
		map.put("Berhasil", Boolean.TRUE);
		map.put("Notifikasi", "Data Berhasil Diupdate");
		return map;
	}
	
	@DeleteMapping("/delete/{namaDosen}")
	@ResponseStatus(code=HttpStatus.GONE)
	public Map<String, Object> deleteApi(@PathVariable String namaDosen){
		this.dosenService.delete(dosenRepository.searchNamaDosen(namaDosen));
		Map<String, Object> map= new HashMap<String, Object>();
		map.put("Berhasil", Boolean.TRUE); 
		map.put("Notifikasi", "Data Berhasil Didelete");
		return map;		
	}
	
}
