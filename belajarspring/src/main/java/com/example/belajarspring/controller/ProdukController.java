package com.example.belajarspring.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.example.belajarspring.model.ProdukModel;
import com.example.belajarspring.service.ProdukService;

@Controller
public class ProdukController {
	
	@Autowired
	private ProdukService produkService;
	
	@RequestMapping(value = "menu_tambah_produk")
	public String menuTambahProduk() {
		String html ="produk/tambah_produk";
		return html;
	}
	
	@RequestMapping(value = "proses_tambah_produk")
	public String prosesTambahProduk(HttpServletRequest request, Model model) {
		String kodeProduk = request.getParameter("kode");
		String namaProduk = request.getParameter("nama");
		int hargaProduk = Integer.valueOf(request.getParameter("harga"));
		
		ProdukModel produkModel = new ProdukModel();

		produkModel.setKodeProduk(kodeProduk);
		produkModel.setNamaProduk(namaProduk);
		produkModel.setHargaProduk(hargaProduk);
		
		produkService.create(produkModel);
		model.addAttribute("produkModel", produkModel);
		
		String html ="produk/success";
		return html;
	}
	
	@RequestMapping(value="tampil_produk")
	public String tampilProduk(Model model) {
		
		List<ProdukModel> produkModelList = new ArrayList<ProdukModel>();
		produkModelList = produkService.tampilProduk();
		model.addAttribute("produkModelList", produkModelList);
		
		String html = "produk/tampilProduk";
		return html;
	}
	
	@RequestMapping(value="cari_aja_sendiri")
	public String cariProduk(HttpServletRequest request, Model model) {
		
		String namaProduk = request.getParameter("cariNama");
		
		List<ProdukModel> produkModelList = new ArrayList<ProdukModel>();
		produkModelList = produkService.cariProduk(namaProduk);
		
		model.addAttribute("produkModelList", produkModelList);
		
		String html = "produk/tampilProduk";
		return html;
	}
	
	@RequestMapping(value="daftar_produk")
	public String menuDaftarProduk(Model model) {
		List<ProdukModel> produkModelList = new ArrayList<ProdukModel>();
		produkModelList = produkService.hargaProdukOrder();
		
		model.addAttribute("produkModelList", produkModelList);
		
		String html ="produk/daftar_produk";
		return html;
	}

}
