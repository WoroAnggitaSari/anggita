package com.example.belajarspring.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.belajarspring.model.PresidenModel;

public interface PresidenRepository extends JpaRepository<PresidenModel, String>{

}
