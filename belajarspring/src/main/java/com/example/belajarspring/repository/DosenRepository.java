package com.example.belajarspring.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.example.belajarspring.model.DosenModel;


//  awalnya  public class DosenRepository{} dibuat menjadi abstrak kelas
public interface DosenRepository extends JpaRepository<DosenModel, String> {
	
	@Query("SELECT D FROM DosenModel D ORDER BY D.namaDosen DESC")
	List<DosenModel> querySelectAllOrderNamaDesc();
	
	@Query("SELECT D FROM DosenModel D ORDER BY D.namaDosen ASC")
	List<DosenModel> querySelectAllOrderNamaAsc();
	
	@Query("SELECT D FROM DosenModel D ORDER BY D.usiaDosen DESC")
	List<DosenModel> querySelectAllOrderUsiaDesc();
	
	@Query("SELECT D FROM DosenModel D ORDER BY D.usiaDosen ASC")
	List<DosenModel> querySelectAllOrderUsiaAsc();
	
	@Query("SELECT D FROM DosenModel D WHERE D.namaDosen LIKE %?1% ")
	List<DosenModel> querySelectAllWhereNamaLike(String katakunci_nama);
	
	@Query("SELECT D FROM DosenModel D WHERE D.usiaDosen =?1 AND D.namaDosen LIKE '%a'")
	List<DosenModel> update(int katakunci_usia);
	
	
	
	
	
	
	
	@Query("SELECT D FROM DosenModel D WHERE D.usiaDosen = ?1 ")
	List<DosenModel> querySelectAllWhereUsiaEqual(int katakunci_nama);
	
	@Query("SELECT D FROM DosenModel D WHERE D.usiaDosen < ?1 ")
	List<DosenModel> querySelectAllWhereUsiaKurang(int katakunci_nama);
	
	@Query("SELECT D FROM DosenModel D WHERE D.usiaDosen <= ?1 ")
	List<DosenModel> querySelectAllWhereUsiaSamaKurang(int katakunci_nama);
	
	@Query("SELECT D FROM DosenModel D WHERE D.usiaDosen > ?1 ")
	List<DosenModel> querySelectAllWhereUsiaLebihbesar(int katakunci_nama);
	
	@Query("SELECT D FROM DosenModel D WHERE D.usiaDosen >= ?1 ")
	List<DosenModel> querySelectAllWhereUsiaSamaLebih(int katakunci_nama);
	
	@Query("SELECT D FROM DosenModel D WHERE D.usiaDosen != ?1 ")
	List<DosenModel> querySelectAllWhereUsiaTidakSama(int katakunci_nama);
	
	@Query("SELECT D FROM DosenModel D WHERE D.namaDosen != ?1 ")
	DosenModel queryWhereID(String namaDosen);
	
	@Query("SELECT D FROM DosenModel D WHERE D.namaDosen =?1")
	DosenModel searchNamaDosen(String namaDosen);
}
