package com.example.belajarspring.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="TBL_PRODUK")
public class ProdukModel {
	
	@Id
	@Column(name="KODE_PRD")
	private String kodeProduk;
	
	@Column(name="NAMA_PRD")
	private String namaProduk;
	
	@Column(name="HARGA_PRD")
	private int hargaProduk;
	
	public String getKodeProduk() {
		return kodeProduk;
	}
	public void setKodeProduk(String kodeProduk) {
		this.kodeProduk = kodeProduk;
	}
	public String getNamaProduk() {
		return namaProduk;
	}
	public void setNamaProduk(String namaProduk) {
		this.namaProduk = namaProduk;
	}
	public int getHargaProduk() {
		return hargaProduk;
	}
	public void setHargaProduk(int hargaProduk) {
		this.hargaProduk = hargaProduk;
	}
	
	

}
