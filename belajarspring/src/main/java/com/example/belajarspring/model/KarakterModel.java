package com.example.belajarspring.model;

public class KarakterModel {
	
	private String namaKarakter;
	private int levelKarakter;
	private String statusKarakter;
	public String getNamaKarakter() {
		return namaKarakter;
	}
	public void setNamaKarakter(String namaKarakter) {
		this.namaKarakter = namaKarakter;
	}
	public int getLevelKarakter() {
		return levelKarakter;
	}
	public void setLevelKarakter(int levelKarakter) {
		this.levelKarakter = levelKarakter;
	}
	public String getStatusKarakter() {
		return statusKarakter;
	}
	public void setStatusKarakter(String statusKarakter) {
		this.statusKarakter = statusKarakter;
	}
	
	
	
}
