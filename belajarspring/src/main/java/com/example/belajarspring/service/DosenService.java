package com.example.belajarspring.service;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.belajarspring.model.DosenModel;
import com.example.belajarspring.repository.DosenRepository;

@Service
@Transactional
public class DosenService {
	
	@Autowired
	private DosenRepository dosenRepository;
	
	// modifier kosong/g namaMethod(TipeVariable namaVar)
	public void create(DosenModel dosenModel) {
		dosenRepository.save(dosenModel);
	}
	
	public List<DosenModel> read() {
		return dosenRepository.findAll();
	}
	
	public List<DosenModel> readOrderNama() {
		return dosenRepository.querySelectAllOrderNamaDesc();
	}
	
	public List<DosenModel> readOrderBy() {
		return dosenRepository.querySelectAllOrderUsiaAsc();
	}
	
	public List<DosenModel> readWhereNama(String katakunci_nama) {
		return dosenRepository.querySelectAllWhereNamaLike(katakunci_nama);
	}

	
	
	public List<DosenModel> readWhereUsia(int katakunci_usia) {
		return dosenRepository.querySelectAllWhereUsiaEqual(katakunci_usia);
	}
	
	
	public List<DosenModel> readWhereUsiaLebih(int katakunci_usia) {
		
				
		return dosenRepository.querySelectAllWhereUsiaLebihbesar(katakunci_usia);

	}
	

	
	public List<DosenModel> readWhereUsiaKurang(int katakunci_usia) {
		return dosenRepository.querySelectAllWhereUsiaKurang(katakunci_usia);

	}

	
	public List<DosenModel> CariParameter(int katakunci_usia, String katakunci_operator){
		
		List<DosenModel> DosenModelList = new ArrayList<DosenModel>();
		if (katakunci_operator.equals("=") ) {
			DosenModelList = dosenRepository.querySelectAllWhereUsiaEqual(katakunci_usia);
		} 
		
		else if(katakunci_operator.equals(">")) {
			DosenModelList = dosenRepository.querySelectAllWhereUsiaLebihbesar(katakunci_usia);
			}
		
		else if(katakunci_operator.equals(">=")) {
			DosenModelList = dosenRepository.querySelectAllWhereUsiaSamaLebih(katakunci_usia);
			}
		
		else if(katakunci_operator.equals("<=")) {
			DosenModelList = dosenRepository.querySelectAllWhereUsiaSamaKurang(katakunci_usia);
			}
		else  {
			DosenModelList = dosenRepository.querySelectAllWhereUsiaTidakSama(katakunci_usia);
			}
		
		
		return DosenModelList;

		
	}
	
	public void update(DosenModel dosenModel) {
		dosenRepository.save(dosenModel);
	}
	
	
	public DosenModel readByString(String namaDosen) {
		return dosenRepository.queryWhereID(namaDosen);
	}
	
//belum selesai
	public void delete(DosenModel dosenModel) {
		 dosenRepository.delete(dosenModel);
	}

	
	
	
	
	
	
	
	
	
	
}
