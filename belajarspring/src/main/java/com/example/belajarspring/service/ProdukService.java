package com.example.belajarspring.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.belajarspring.model.ProdukModel;
import com.example.belajarspring.repository.ProdukRepository;

@Service
@Transactional
public class ProdukService {
	
	@Autowired
	private ProdukRepository produkRepository;
	
	public void create(ProdukModel produkModel) {
		produkRepository.save(produkModel);
	}
	
	public List<ProdukModel> tampilProduk(){
		return produkRepository.findAll();
	}
	
	public List<ProdukModel> cariProduk(String namaProduk){
		return produkRepository.cariSemuaDataProduk(namaProduk);
	}
	
	public List<ProdukModel> hargaProdukOrder() {
		return produkRepository.hargaProdukOrderAsc();
	}
}